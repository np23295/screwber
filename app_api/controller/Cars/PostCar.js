var md5 = require("MD5");
var db = require('../../models');
var jwt = require('jsonwebtoken');
var secret = require('../../utility/secret');
var sendJSONresponse = require('../../utility/response');
var verifyToken = require('../User/VerifyToken');


// Add New Car
module.exports.postCar = function (req, res) {
    // var access_token = req.body.access_token;
    var car = {
        make: req.body.make,
        model: req.body.model,
        year: req.body.year,
        number_plate: req.body.number_plate,
        total_seats: req.body.total_seats,
        user_id: req.body.user_id,
    };
    console.log(car);
    // if (access_token) {
        // var token_result = verifyToken(access_token);
        // console.log(verifyToken(access_token));
        // console.log(token_result);

        // TODO token validation not working

        // token_result is token validation response
        // if (token_result) {
        db.car.create(car).then(function (result) {
            if (result) {
                db.car.findAll({ where: { 'user_id': car.user_id } }).then(cars => {
                    sendJSONresponse(res, 200, true, cars, '');
                }).catch(function (err) {
                    console.log(err);
                    sendJSONresponse(res, 400, false, {}, err.name);
                });
            }
            
            // Generate Token and Set 
        }).catch(function (err) {
            console.log(err);
            sendJSONresponse(res, 400, false, {}, err.name);
        });
    // }

};

