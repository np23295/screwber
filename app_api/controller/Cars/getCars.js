var md5 = require("MD5");
var db = require('../../models');
var jwt = require('jsonwebtoken');
var secret = require('../../utility/secret');
var sendJSONresponse = require('../../utility/response');
var verifyToken = require('../User/VerifyToken');


  module.exports.getCars = function(req, res) {
    if (req.params.id){
        db.car.findAll({ where: { 'user_id': req.params.id } }).then(cars => {
            sendJSONresponse(res, 200, true, cars, '');
        }).catch(function (err) {
            console.log(err);
            sendJSONresponse(res, 400, false, {}, err.name);
        });
    }else{
        sendJSONresponse(res, 400, false, {}, "Id Not Found.");
    }
  };