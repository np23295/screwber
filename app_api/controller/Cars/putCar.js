var md5 = require("MD5");
var db = require('../../models');
var jwt = require('jsonwebtoken');
var secret = require('../../utility/secret');
var sendJSONresponse = require('../../utility/response');
var verifyToken = require('../User/VerifyToken');



/* PUT */
module.exports.putCars = function(req, res) {
    if (req.params.id){
        db.car.findOne({ where: { 'car_id': req.params.id } }).then(cars => {
            if (cars){
                var car = {
                    make: req.body.make,
                    model: req.body.model,
                    year: req.body.year,
                    number_plate: req.body.number_plate,
                    total_seats: req.body.total_seats,
                    user_id: req.body.user_id,
                };
                cars.update(car).then(function (result) {
                    if (result) {
                        sendJSONresponse(res, 200, true, result, '');
                    }
                    // Generate Token and Set 
                }).catch(function (err) {
                    console.log(err);
                    sendJSONresponse(res, 400, false, {}, err.name);
                });
            }
        }).catch(function (err) {
            console.log(err);
            sendJSONresponse(res, 400, false, {}, err.name);
        });
    }
    else {
        sendJSONresponse(res, 400, false, {}, "Id Not Found.");
        return;
    }
  };