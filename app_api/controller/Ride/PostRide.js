var md5 = require("MD5");
var db = require('../../models');
var jwt = require('jsonwebtoken');
var secret = require('../../utility/secret');
var sendJSONresponse = require('../../utility/response');


// Add New User
module.exports.postRide = function (req, res) {
    // var access_token = req.body.access_token;
    var ride = {
        origin_latitude : req.body.origin_latitude,
        origin_longitude : req.body.origin_longitude,
        dest_latitude : req.body.dest_latitude,
        dest_longitude : req.body.dest_longitude, 
        amount : req.body.amount,
        user_id : req.body.user_id,
        date : req.body.date,
        car_id : req.body.car_id,
        total_seats : req.body.total_seats,
    };
    db.ride.create(ride).then(function (result) {
        sendJSONresponse(res, 200, true, result , '');
    }).catch(function (err){
        console.log(err);
        sendJSONresponse(res, 400, false, {} , err.name);
    });
};

