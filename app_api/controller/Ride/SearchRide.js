var md5 = require("MD5");
var db = require('../../models');
var jwt = require('jsonwebtoken');
var secret = require('../../utility/secret');
var sendJSONresponse = require('../../utility/response');

const Sequelize = require('sequelize');
const Op = Sequelize.Op;
// Add New User
module.exports.searchRide = function (req, res) {
    console.log(req.params);
    var search_ride = {
        origin_latitude : {[Op.like]: req.params.origin_latitude.split(".")[0]+"."+ "%"},
        origin_longitude : {[Op.like]: req.params.origin_longitude.split(".")[0]+"." + "%"},
        dest_latitude : {[Op.like]: req.params.dest_latitude.split(".")[0]+"." + "%"},
        dest_longitude : {[Op.like]: req.params.dest_longitude.split(".")[0]+"." + "%"},
        date : {
            [Op.gte]: "%" + req.params.date + "%",
          },
    };
    console.log(search_ride);
    
    db.ride.findAll({ where: search_ride, order: [
        ['date', 'ASC']
    ], include: [
        db.car, db.user
    ] }).then(function (result) {
        console.log(result);
        sendJSONresponse(res, 200, true, result , '');
    }).catch(function (err){
        console.log(err);
        sendJSONresponse(res, 400, false, {} , err.name);
    });
};

