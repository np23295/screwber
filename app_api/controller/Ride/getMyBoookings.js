var md5 = require("MD5");
var db = require('../../models');
var jwt = require('jsonwebtoken');
var secret = require('../../utility/secret');
var sendJSONresponse = require('../../utility/response');

const Sequelize = require('sequelize');
const Op = Sequelize.Op;
// Add New User
module.exports.getBookings = function (req, res) {
    console.log(req.params);
    db.ride.findAll({ where: {'user_id': req.params.user_id}, group: ['ride_id']}).then(function (result) {
        console.log(result);
        sendJSONresponse(res, 200, true, result , '');
    }).catch(function (err){
        console.log(err);
        sendJSONresponse(res, 400, false, {} , err.name);
    });
};

