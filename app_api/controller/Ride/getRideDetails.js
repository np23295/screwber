var md5 = require("MD5");
var db = require('../../models');
var jwt = require('jsonwebtoken');
var secret = require('../../utility/secret');
var sendJSONresponse = require('../../utility/response');

const Sequelize = require('sequelize');
const Op = Sequelize.Op;
// Add New User
module.exports.getRideDetails = function (req, res) {
    db.ride.findOne({ where: {'ride_id': req.params.ride_id}, include: [
        db.car] }).then(function (result) {
        sendJSONresponse(res, 200, true, result , '');
    }).catch(function (err){
        console.log(err);
        sendJSONresponse(res, 400, false, {} , err.name);
    });
};

