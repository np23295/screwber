var md5 = require("MD5");
var db = require('../../models');
var jwt = require('jsonwebtoken');
var secret = require('../../utility/secret');
var sendJSONresponse = require('../../utility/response');

const Sequelize = require('sequelize');
const Op = Sequelize.Op;
// Add New User
module.exports.bookRide = function (req, res) {
    console.log(req.body);
    var booking = {
        user_id : req.body.user_id,
        ride_id : req.body.ride_id,       
    };
    db.booking.create(booking).then(function (result) {
        sendJSONresponse(res, 200, true, result , '');
    }).catch(function (err){
        console.log(err);
        sendJSONresponse(res, 400, false, {} , err.name);
    });
};

