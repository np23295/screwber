var jwt = require('jsonwebtoken');
var secret = require('../../utility/secret');

// Token Generation
module.exports.generate_token = function (row){
    var token = jwt.sign(row, secret.secret, {
        expiresIn: 1440
    });
    return {'access_token': token};
};
