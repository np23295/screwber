var md5 = require("MD5");
var verifyToken = require('../User/VerifyToken');
var db = require("../../models");
var sendJSONresponse = require('../../utility/response');
var jwt = require('jsonwebtoken');
var secret = require('../../utility/secret');

module.exports.userSignIn = function (req, res) {
    var email = req.body.email;
    var password = req.body.password;
    if (email && password) {
        var encryted_password = md5(password);
        var user_details = { 'email': email, 'password': encryted_password };
        db.user.findOne({ where: user_details }).then(user => {
            if (user) {
                db.access_token.findOne({ where: { 'user_id': user.user_id } }).then(access_token => {
                    if (access_token) {
                        var token_result = verifyToken(access_token);
                        // token_result is token validation response
                        if (token_result) {
                            // if token is valid
                            var final_response = {};
                            for (var key in user.dataValues) {
                                if (key != 'password') {
                                    final_response[key] = user.dataValues[key];
                                }
                            }
                            final_response['access_token'] = access_token;
                            sendJSONresponse(res, 200, true, final_response, '');
                        } else {
                            // Generate Token and Set it.
                            var user_data = {
                                first_name: user.dataValues.first_name,
                                last_name: user.dataValues.last_name,
                                email: user.dataValues.email,
                                password: user.dataValues.password,
                                role: user.dataValues.role,
                            };
                            var access_token = jwt.sign(user_data, secret.secret, {
                                expiresIn: 1440
                            });
                            var final_response = {};
                            for (var key in user.dataValues) {
                                if (key != 'password') {
                                    final_response[key] = user.dataValues[key];
                                }
                            }
                            final_response['access_token'] = access_token;
                            var response = { 'access_token': access_token };
                            sendJSONresponse(res, 200, true, final_response, '');
                            response['user_id'] = user.user_id;
                            db.access_token.create(response).then(function (result_response) {
                                console.log("Access Token Set");
                            });
                        }
                    } else {

                    }
                });
                // sendJSONresponse(res, 201, true, response, '');
            } else {
                sendJSONresponse(res, 400, false, {} , "Invalid Username or password");
            }
        }).catch(function (err){
            console.log(err);
            sendJSONresponse(res, 400, false, {} , err.name);
        });
    } else {
        sendJSONresponse(res, 400, false, {} , "Email or password is empty");
    }
};
