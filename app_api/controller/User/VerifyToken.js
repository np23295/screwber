var db = require('../../models');
var jwt = require('jsonwebtoken');
var secret = require('../../utility/secret');
var sendJSONresponse = require('../../utility/response');

var verifyToken = function (token) {
	if (token) {
		// verify secret and checks exp
		jwt.verify(token, secret.secret, function (err, currUser) {
			if (err) {
				// Return False on Error
				console.log(err);
				return false;
			} else {
				console.log(currUser)
				// decoded object
                return true;
                
			}
		});
	}
	 else {
		// send not found error
		return false;
	}
};
module.exports = verifyToken;


module.exports.verifyApiToken = function (req, res){
	if (req.body.access_token){
		if(verifyToken(req.body.access_token)){
			sendJSONresponse(res, 200, true, '', '');
		}else{
			sendJSONresponse(res, 401, false, '', 'Token is Expired');
		}
	}else{
		sendJSONresponse(res, 204, false, '', 'No Token Found');
	}
}

