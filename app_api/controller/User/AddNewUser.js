var md5 = require("MD5");
var db = require('../../models');
var jwt = require('jsonwebtoken');
var secret = require('../../utility/secret');
var sendJSONresponse = require('../../utility/response');


// Add New User
module.exports.addNewUser = function (req, res) {
    var user = {
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        email: req.body.email,
        password: md5(req.body.password),
        role: req.body.role,
    };
    db.user.create(user).then(function (result) {
        var token = jwt.sign(user, secret.secret, {
            expiresIn: 1440
        });
        var final_response = {};
        for (var key in result.dataValues) {
            if(key != 'password'){
                final_response[key] = result.dataValues[key];
            }
        }
        final_response['access_token'] = token;
        var response =  {'access_token': token};
        sendJSONresponse(res, 201, true, final_response, '');
        response['user_id'] = result.user_id;
        db.access_token.create(response).then(function (result_response) {
            console.log("Access Token Set");
        });
    }).catch(function (err){
        console.log(err);
        sendJSONresponse(res, 400, false, {} , err.name);
    });
};

