var express = require('express');
var router = express.Router();
var crtl_addNewUser = require('../controller/User/AddNewUser');
// var  findAllUser = require('../controllers/findAllUsers');
var  ctrlVerifyToken = require('../controller/User/VerifyToken');
var ctrlLogin = require('../controller/User/UserLogin');
// Controller for Ride
var ctrlPostRide = require('../controller/Ride/PostRide');
var ctrlSearchRide = require('../controller/Ride/SearchRide');
var ctrlBookRide = require('../controller/Ride/BookRide');
var ctrlMyBookings = require('../controller/Ride/getMyBoookings');
var ctrlRideDetails = require('../controller/Ride/getRideDetails');
// Controllers for Car
var ctrlPostCar = require('../controller/Cars/PostCar');
var ctrlGetCars = require('../controller/Cars/getCars');
var ctrlPutCars = require('../controller/Cars/putCar');

// Post New User
router.post('/user', crtl_addNewUser.addNewUser);

// // Get User by ID
// // router.get('/food/:id', ctrlFood.getFoodById);

// Verify User Token
router.post('/user/verify_token', ctrlVerifyToken.verifyApiToken);

// User Login Check
router.post('/user/login', ctrlLogin.userSignIn);

// Post a Ride
router.post('/ride', ctrlPostRide.postRide);
router.post('/ride/book', ctrlBookRide.bookRide);
router.get('/ride/:origin_latitude&:origin_longitude&:dest_latitude&:dest_longitude&:date', ctrlSearchRide.searchRide);
router.get('/ride/book/:user_id', ctrlMyBookings.getBookings);
router.get('/ride/details/:ride_id', ctrlRideDetails.getRideDetails);

// Post a Car
router.post('/car', ctrlPostCar.postCar);
router.get('/car/:id', ctrlGetCars.getCars);
router.put('/car/:id', ctrlPutCars.putCars);


// // router.delete('/food/:id', ctrlFood.deleteFood);
// // router.put('/food/:id', ctrlFood.putFood);

module.exports = router;