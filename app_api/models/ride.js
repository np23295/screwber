
module.exports = function (sequelize, DataTypes) {
    // User Model Defination
    var modelDefinition = {
        ride_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },

        origin_latitude: {
            type: DataTypes.STRING,
            allowNull: false
        },

        origin_longitude: {
            type: DataTypes.STRING,
            allowNull: false
        },

        dest_latitude: {
            type: DataTypes.STRING,
            allowNull: false
        },

        dest_longitude: {
            type: DataTypes.STRING,
            allowNull: false
        },

        date: {
            type: DataTypes.DATEONLY,
            defaultValue: DataTypes.NOW
        },
        
        total_seats: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 1
        },
    };

    // Creating Model
    var ride = sequelize.define('ride', modelDefinition);
    // ride.associate = function (models) {
    //     models.ride.hasMany(models.access_token);
    // };
    ride.associate = function (models) {
        models.ride.belongsTo(models.user, {
          onDelete: "CASCADE",
          foreignKey: 'user_id'
        });
        models.ride.belongsTo(models.car, {
            onDelete: "CASCADE",
            foreignKey: 'car_id'
          });
      };
     
    // Returning Model
    return ride;
}
