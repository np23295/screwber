
module.exports = function (sequelize, DataTypes) {
    // User Model Defination
    var modelDefinition = {
        booking_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
    };

    // Creating Model
    var booking = sequelize.define('booking', modelDefinition);
    booking.associate = function (models) {
        models.booking.belongsTo(models.user, {
            onDelete: "CASCADE",
            foreignKey: 'user_id'
          });
        models.booking.belongsTo(models.ride, {
            onDelete: "CASCADE",
            foreignKey: 'ride_id'
          });
    };
    // Returning Model
    return booking;
}
