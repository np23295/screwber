
module.exports = function (sequelize, DataTypes) {
    // User Model Defination
    var modelDefinition = {
        user_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },

        first_name: {
            type: DataTypes.STRING,
            allowNull: false
        },

        last_name: {
            type: DataTypes.STRING,
            allowNull: false
        },

        dob: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        },

        email: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                isEmail: true
            },
            unique: {
                args: true,
                msg: 'Email address already !'
            }
        },

        is_verified: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: 0
        },

        password: {
            type: DataTypes.STRING,
            allowNull: false
        },

        image_path: {
            type: DataTypes.STRING,
            allowNull: true
        },

        country: {
            type: DataTypes.STRING,
            allowNull: true
        },

        role: {
            type: DataTypes.ENUM('admin', 'driver', 'passenger'),
            defaultValue: 'passenger'
        },

        city: {
            type: DataTypes.STRING,
            allowNull: true
        },

        phone_extension: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 1
        },

        phone_number: {
            type: DataTypes.STRING,
            allowNull: true
        },

        block_status: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: 0
        },
    };

    // Creating Model
    var user = sequelize.define('user', modelDefinition);
    user.associate = function (models) {
        models.user.hasMany(models.access_token);
        models.user.hasMany(models.ride);
    };
    // Returning Model
    return user;
}
