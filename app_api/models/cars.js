
module.exports = function (sequelize, DataTypes) {
    // User Model Defination
    var modelDefinition = {
        car_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },

        make: {
            type: DataTypes.STRING,
            allowNull: false
        },

        model: {
            type: DataTypes.STRING,
            allowNull: false
        },

        year: {
            type: DataTypes.STRING,
            allowNull: false
        },

        number_plate: {
            type: DataTypes.STRING,
            allowNull: false
        },
        
        total_seats: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 1
        },
    };

    // Creating Model
    var car = sequelize.define('car', modelDefinition);
    // ride.associate = function (models) {
    //     models.ride.hasMany(models.access_token);
    // };
    car.associate = function (models) {
        models.car.hasMany(models.ride);
        models.car.belongsTo(models.user, {
            onDelete: "CASCADE",
            foreignKey: 'user_id'
          });
    };

  
    // Returning Model
    return car;
}
