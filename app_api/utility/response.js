
var sendJSONresponse = function (res, status, isSuccess, content, message) {
    res.status(status);
    // Designing the response Structure
    var response;
    if (isSuccess){
        response = {
            'isSuccess' : isSuccess,
            'body': content,
        }
    }else{
        response = {
            'isSuccess' : isSuccess,
            'message':  message
        }
    }
    res.json(response);
};

module.exports = sendJSONresponse;